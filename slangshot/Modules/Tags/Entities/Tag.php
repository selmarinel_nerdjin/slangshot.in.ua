<?php

namespace Modules\Tags\Entities;


use Illuminate\Database\Eloquent\Model;
use Modules\Words\Entities\Word;

class Tag extends Model
{
    protected $table = 'tags';

    protected $fillable = ['name'];

    protected $visible = ['id','name'];

    public function words()
    {
        return $this->belongsToMany(Word::class, 'word_has_tag', 'tag_id', 'word_id');
    }
}