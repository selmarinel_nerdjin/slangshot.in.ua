<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTags extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tags', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->timestamps();
        });

        Schema::create('word_has_tag', function (Blueprint $table) {
            $table->increments('id');
            $table->integer("word_id")->unsigned();
            $table->integer("tag_id")->unsigned();
            $table->timestamps();
        });

        Schema::table('word_has_tag', function (Blueprint $table) {
            $table->foreign('word_id')->references('id')->on('words');
            $table->foreign('tag_id')->references('id')->on('tags');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('word_has_tag');
        Schema::dropIfExists('tags');
    }
}
