<?php
/**
 * Created by PhpStorm.
 * User: Selmarinel
 * Date: 02.03.2018
 * Time: 11:36
 */

namespace Modules\Tags\Service\Exceptions;


use Illuminate\Http\Response;

class TagNotFoundException extends TagsServiceException
{
    public $code = Response::HTTP_NOT_FOUND;
}