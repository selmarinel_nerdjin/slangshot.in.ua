<?php

namespace Modules\Tags\Service;


use Illuminate\Http\Request;
use Modules\Tags\Entities\Tag;
use Modules\Tags\Service\Exceptions\TagNotFoundException;

class Handle
{
    /**
     * @param Request $request
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getTags(Request $request)
    {
        $builder = Tag::query();
        if ($request->has('name')) {
            $builder->where('name', $request->input('name'));
        }
        return $builder->get();
    }

    public function createTagFromRequest(Request $request)
    {
        $tag = new Tag();
        $tag->name = $request->input('name');
        $tag->save();
        return $tag;
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|null|static|static[]
     * @throws TagNotFoundException
     */
    public function updateTagFromRequest(Request $request, $id)
    {
        $tag = Tag::query()->find($id);
        if(!$tag){
            throw new TagNotFoundException;
        }
        $tag->name = $request->input('name');
        $tag->save();
        return $tag;
    }


}