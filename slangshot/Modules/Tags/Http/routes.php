<?php

Route::group([
    'prefix' => 'api',
    'namespace' => 'Modules\Tags\Http\Controllers',
    'middleware' => [
        'authenticate',
        'guarded',
        'manipulate_tags'
    ]
], function () {

    Route::get('/tags', "TagsController@getCollections")->name('api:tags:read');

    Route::post('/tags', "TagsController@create")->name('api:tags:create');
    Route::put('/tags/{id}', "TagsController@update")->name('api:tags:update');
    Route::delete('/tags/{id}', "TagsController@remove")->name('api:tags:delete');

});
