<?php

namespace Modules\Tags\Http\Middleware;


use Closure;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Modules\Users\Service\Roles\AdminRole;
use Modules\Users\Service\Roles\ModeratorRole;
use Modules\Users\Service\User\User;

class CanManipulateTags
{
    public function handle($request, Closure $next)
    {
        $user = new User($request->user());
        if ($user->getRole() instanceof ModeratorRole || $user->getRole() instanceof AdminRole) {
            return $next($request);
        }
        return new JsonResponse(['message' => 'Not Allow'], Response::HTTP_I_AM_A_TEAPOT);

    }
}