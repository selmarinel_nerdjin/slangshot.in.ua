<?php

namespace Modules\Tags\Http\Controllers;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Tags\Service\Exceptions\TagNotFoundException;
use Modules\Tags\Service\Handle;

class TagsController extends Controller
{

    /** @var Handle */
    private $handle;

    public function __construct(Handle $handle)
    {
        $this->handle = $handle;
    }

    public function getCollections(Request $request)
    {
        $collection = $this->handle->getTags($request);
        return new JsonResponse([
            'count' => $collection->count(),
            'collection' => $collection
        ]);
    }

    public function create(Request $request)
    {
        $tag = $this->handle->createTagFromRequest($request);
        return new JsonResponse([
            'tag' => $tag
        ]);
    }

    public function update(Request $request, $id)
    {
        try {
            $tag = $this->handle->updateTagFromRequest($request, $id);
            return new JsonResponse(['tag' => $tag]);
        } catch (TagNotFoundException $tagNotFoundException) {
            return new JsonResponse(['error' => 'Tag not found'], $tagNotFoundException->getCode());
        }
    }

    public function remove(Request $request)
    {
        return new JsonResponse(['error' => 'Not available'], Response::HTTP_BAD_REQUEST);
    }
}
