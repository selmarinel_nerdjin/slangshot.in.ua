<?php

namespace Modules\Words\Entities;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Modules\Tags\Entities\Tag;
use Modules\Users\Entities\UserModel;

/**
 * @property \Carbon\Carbon $created_at
 * @property int $id
 * @property \Carbon\Carbon $updated_at
 * @property UserModel|null $author
 * @property string $letter
 * @property array $attributes
 * @property string $description
 */
class Word extends Model
{
    const WORD_HIDE = 0;
    const WORD_SUBMITTED = 2;
    const WORD_ACTIVE = 1;
    const WORD_DELETE = 3;

    protected $fillable = [
        "name",
        "description",
        "letter",
        "id",
        "user_id",
        "status"
    ];

    protected $hidden = [
        "updated_at",
        "user_id",
        "pivot"
    ];

    public function author()
    {
        return $this->hasOne(UserModel::class, "id", "user_id");
    }

    public function tags()
    {
        return $this->belongsToMany(Tag::class, 'word_has_tag', 'word_id', 'tag_id');
    }

    public function synonyms()
    {
        return $this->belongsToMany(Word::class,'word_to_word','parent_id','child_id');
    }
}