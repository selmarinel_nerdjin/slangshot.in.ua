<?php

namespace Modules\Words\Entities;


use Illuminate\Database\Eloquent\Model;

class WordToWord extends Model
{
    protected $table = 'word_to_word';

    protected $fillable = ['parent_id', 'child_id'];

    public function parent()
    {
        return $this->hasOne(Word::class, 'id', 'parent_id');
    }

    public function child()
    {
        return $this->hasOne(Word::class, 'id', 'child_id');
    }

    public static function createDirectRelation(int $paren_id, int $child_id)
    {
        $relate = new self();
        $relate->parent_id = $paren_id;
        $relate->child_id = $child_id;
        $relate->save();
        return $relate;
    }

    public static function createReverseRelation(int $paren_id, int $child_id)
    {
        $relate = new self();
        $relate->parent_id = $child_id;
        $relate->child_id = $paren_id;
        $relate->save();
        return $relate;
    }
}