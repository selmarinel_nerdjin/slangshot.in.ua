<?php
/**
 * Created by PhpStorm.
 * User: selma
 * Date: 15.02.2018
 * Time: 13:39
 */

namespace Modules\Words\Service\Exceptions;


use Illuminate\Http\Response;

class WordValidationException extends \Exception
{
    public $message = 'Validation Error';

    public $code = Response::HTTP_BAD_REQUEST;
}