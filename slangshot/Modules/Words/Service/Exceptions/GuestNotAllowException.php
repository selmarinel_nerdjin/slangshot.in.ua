<?php
/**
 * Created by PhpStorm.
 * User: selma
 * Date: 15.02.2018
 * Time: 12:29
 */

namespace Modules\Words\Service\Exceptions;


use Illuminate\Http\Response;

class GuestNotAllowException extends \Exception
{

    public $message = 'Guests can\'t create words.';

    public $code = Response::HTTP_BAD_REQUEST;
}