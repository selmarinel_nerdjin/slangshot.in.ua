<?php

namespace Modules\Words\Service\Exceptions;

use Illuminate\Http\Response;

class NotFoundWordException extends \Exception
{

    public $message = 'Word not found';

    public $code = Response::HTTP_NOT_FOUND;
}