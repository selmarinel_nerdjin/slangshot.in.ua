<?php

namespace Modules\Words\Service;

use Illuminate\Http\Request;
use Modules\Users\Service\User\User;
use Modules\Words\Entities\Word;
use Modules\Words\Service\Builder\GetterBuilder;
use Modules\Words\Service\Builder\SetterBuilder;
use Modules\Words\Service\Exceptions\GuestNotAllowException;

class Handle
{
    /** @var User */
    private $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function getById($id)
    {
        $builder = new GetterBuilder($this->user);
        return $builder->findById($id);
    }

    public function getByName($name)
    {
        $builder = new GetterBuilder($this->user);
        return $builder->findByName($name);
    }

    public function getNearest($word)
    {
        $builder = new GetterBuilder($this->user);
        $builder->ordering();
        return $builder->near($word);
    }

    public function getCollection(Request $request)
    {
        $builder = new GetterBuilder($this->user);

        if ($request->input('word')) {
            $word = $request->input('word');
            return $builder->near($word);
        }

        if ($request->input('take')) {
            $take = ((int)$request->input('take')) ?: 10;
            $builder->setLimit($take);
        }

        if ($request->input('letter')) {
            $letter = mb_substr($request->input('letter'), 0, 1);
            $builder->byLetter($letter);
        }

        if ($request->input('status') !== null) {
            $status = (int)$request->input('status');
            $builder->byStatus($status);
        }

        if ($request->input("tag")) {
            $builder->byTag($request->input('tag'));
        }

        return $builder->getCollection();
    }

    /**
     * @param Request $request
     *
     * @return \Modules\Words\Entities\Word
     * @throws \Modules\Users\Service\Exceptions\UserPermissionException
     */
    public function save(Request $request)
    {
        $builder = new SetterBuilder($this->user);
        return $builder->create($request);
    }

    /**
     * @param Request $request
     * @param         $id
     *
     * @return \Modules\Words\Entities\Word
     * @throws Exceptions\NotFoundWordException
     * @throws Exceptions\WordValidationException
     * @throws \Modules\Users\Service\Exceptions\UserPermissionException
     */
    public function update(Request $request, $id)
    {
        $builder = new SetterBuilder($this->user);
        return $builder->update($request, $id);
    }

    /**
     * @param $id
     *
     * @return bool
     * @throws Exceptions\NotFoundWordException
     * @throws \Modules\Users\Service\Exceptions\UserPermissionException
     */
    public function delete($id)
    {
        $builder = new SetterBuilder($this->user);
        return $builder->delete($id);
    }

    /**
     * @param $id
     * @return Word
     * @throws Exceptions\NotFoundWordException
     * @throws \Modules\Users\Service\Exceptions\UserPermissionException
     */
    public function approve($id)
    {
        $builder = new SetterBuilder($this->user);
        return $builder->setStatus($id, Word::WORD_ACTIVE);
    }

    /**
     * @param $id
     * @return Word
     * @throws Exceptions\NotFoundWordException
     * @throws \Modules\Users\Service\Exceptions\UserPermissionException
     */
    public function hide($id)
    {
        $builder = new SetterBuilder($this->user);
        return $builder->setStatus($id, Word::WORD_HIDE);
    }
}