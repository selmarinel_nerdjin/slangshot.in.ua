<?php

namespace Modules\Words\Service\Builder;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Modules\Users\Service\Exceptions\UserPermissionException;
use Modules\Users\Service\Roles\AdminRole;
use Modules\Users\Service\Roles\GuestRole;
use Modules\Users\Service\Roles\ModeratorRole;
use Modules\Users\Service\User\User;
use Modules\Words\Entities\Word;
use Modules\Words\Entities\WordToWord;
use Modules\Words\Service\Exceptions\GuestNotAllowException;
use Modules\Words\Service\Exceptions\NotFoundWordException;
use Modules\Words\Service\Exceptions\WordValidationException;

class SetterBuilder
{
    /**
     * @var User
     */
    private $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * @param Request $request
     *
     * @return Word
     * @throws UserPermissionException
     */
    public function create(Request $request)
    {
        if ($this->user->getRole() instanceof AdminRole || $this->user->getRole() instanceof ModeratorRole) {
            $word = new Word();
            $word->name = $request->input('name');
            $word->description = $request->input('description');
            $word->status = Word::WORD_SUBMITTED;
            $word->user_id = $this->user->getId();
            /** Another fields */
            $word->save();
            $this->saveSynonyms($word, $request);
            return $word;
        }
        throw new UserPermissionException;
    }

    /**
     * @param Request $request
     * @param         $id
     *
     * @return Word
     * @throws NotFoundWordException
     * @throws UserPermissionException
     * @throws WordValidationException
     */
    public function update(Request $request, $id)
    {

        $word = $this->getWord($id, true);
        if (!$word) {
            throw new NotFoundWordException;
        }
        if ($this->user->getRole() instanceof AdminRole
            || $this->user->getRole() instanceof ModeratorRole
            || $this->user->getId() === $word->id
        ) {
            try {
                $word->description = $request->input('description');
                $word->name = $request->input('name');
                /** another fields */
                $word->save();
                $this->saveSynonyms($word, $request);
                return $word;
            } catch (QueryException $exception) {
                throw new WordValidationException;
            }
        }
        throw new UserPermissionException;
    }

    /**
     * @param $id
     *
     * @return bool
     * @throws NotFoundWordException
     * @throws UserPermissionException
     */
    public function delete($id)
    {
        if ($this->user->getRole() instanceof AdminRole || $this->user->getRole() instanceof ModeratorRole) {
            /** @var Word $word */
            $word = $this->getWord($id);
            if (!$word) {
                throw new NotFoundWordException;
            }
            $word->status = Word::WORD_DELETE;
            return $word->save();
        }
        throw new UserPermissionException;
    }

    /**
     * @param Builder $builder
     * @param bool $moderatorCan
     * @return $this|Builder
     */
    public function whereStatus(Builder $builder, $moderatorCan = false)
    {
        if ($this->user->getRole() instanceof AdminRole) {
            return $builder;
        }
        if ($this->user->getRole() instanceof ModeratorRole && $moderatorCan) {
            return $builder;
        }
        return $builder->where('user_id', $this->user->getId());
    }

    /**
     * @param $id
     *
     * @return Word|null
     */
    public function getWord($id): ?Word
    {
        $builder = (new Word())->newQuery();
        $this->whereStatus($builder);
        /** @var Word $word */
        $word = $builder->where('id', $id)->first();
        return $word;
    }

    /**
     * @param $id
     * @param $status
     *
     * @return Word
     * @throws UserPermissionException
     * @throws NotFoundWordException
     */
    public function setStatus($id, $status): Word
    {
        if ($this->user->getRole() instanceof AdminRole || $this->user->getRole() instanceof ModeratorRole) {
            $word = $this->getWord($id);
            if (!$word) {
                throw new NotFoundWordException;
            }
            $word->status = $status;
            $word->save();
            return $word;
        }
        throw new UserPermissionException;
    }

    /**
     * @param Word $word
     * @param Request $request
     */
    public function saveSynonyms(Word $word, Request $request)
    {
        if ($request->has('synonyms') && count($request->input('synonyms'))) {
            foreach ($request->input('synonyms') as $synonymId) {
                WordToWord::createDirectRelation((int)$word->id, (int)$synonymId);
                WordToWord::createReverseRelation((int)$word->id, (int)$synonymId);
            }
        }
    }
}