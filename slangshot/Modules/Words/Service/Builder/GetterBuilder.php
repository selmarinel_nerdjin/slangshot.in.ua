<?php

namespace Modules\Words\Service\Builder;

use Modules\Users\Service\Roles\AdminRole;
use Modules\Users\Service\Roles\GuestRole;
use Modules\Users\Service\Roles\ModeratorRole;
use Modules\Users\Service\User\User;
use Modules\Words\Entities\Word;
use Illuminate\Database\Eloquent\Builder as EBuilder;

class GetterBuilder
{
    /** @var EBuilder */
    private $builder;
    /** @var array */
    private $relations = [
        'tags',
        'synonyms',
        'author'
    ];
    /** @var User */
    private $user;
    /** @var int */
    private $limit = 10;


    public function __construct(User $user)
    {
        $this->builder = (new Word())->newQuery()->with($this->getRelations());
        $this->user = $user;
    }

    public function findById($id)
    {
        $this->whereStatus();
        return $this->builder->find($id);
    }

    public function findByName($name)
    {
        $this->whereStatus();
        return $this->builder->where('name', $name)->first();
    }

    public function searchByName($name)
    {
        $this->whereStatus();
        return $this->builder
            ->where('name', 'like', "%$name%")
            ->take($this->getLimit())
            ->get();
    }

    public function getCollection()
    {
        $this->whereStatus();
        return $this->builder->orderBy('name')->take($this->getLimit())->get();
    }

    public function near($name)
    {
        $this->whereStatus();
        $builderLess = clone $this->builder;
        $builderMore = clone $this->builder;
        $builderLess->where('name', '<', $name);
        $builderMore->where('name', '>', $name);

        $this->builder->where('name', $name)
            ->union($builderLess->orderByDesc('name')->take(ceil($this->getLimit() / 2)))
            ->union($builderMore->take(ceil($this->getLimit() / 2)))
            ->orderBy('name');
        return $this->builder->get();

    }

    public function randomize()
    {
        $this->builder->inRandomOrder();
    }

    public function ordering(string $desc = 'asc')
    {
        $this->builder->orderBy('name', $desc);
    }

    public function byLetter(string $letter)
    {
        $this->builder->where('letter', $letter);
    }

    public function byStatus(int $status)
    {
        $this->builder->where('status', $status);
    }

    public function getBuilder()
    {
        return $this->builder;
    }

    public function reloadBuilder()
    {
        return $this->builder = (new Word())->newQuery()->with($this->getRelations());
    }

    public function setRelations(array $relations)
    {
        $this->relations = $relations;
    }

    public function getRelations()
    {
        return $this->relations;
    }

    public function setLimit($limit)
    {
        $this->limit = $limit;
    }

    public function getLimit()
    {
        return $this->limit;
    }

    public function byTag($tag)
    {
        return $this->builder->whereHas('tags', function (EBuilder $builder) use ($tag) {
            return $builder->where('name', $tag);
        });
    }

    public function whereStatus()
    {
        $user = $this->user;

        if ($user->getRole() instanceof AdminRole) {
            return $this->builder;
        }

        if ($user->getRole() instanceof ModeratorRole) {
            return $this->builder->where('status', '!=', Word::WORD_DELETE);
        }
        if ($user->getRole() instanceof GuestRole) {
            return $this->builder->where('status', Word::WORD_ACTIVE);
        }
        return $this->builder->where(function (EBuilder $builder) use ($user) {
            $builder->where('status', Word::WORD_ACTIVE)
                ->orWhere(function (EBuilder $query) use ($user) {
                    $query->where('status', Word::WORD_SUBMITTED)
                        ->where('user_id', $user->getId());
                });
        });
    }

}
