<?php
/**
 * Created by PhpStorm.
 * User: selma
 * Date: 15.02.2018
 * Time: 12:43
 */

namespace Modules\Words\Observers;


use Modules\Words\Entities\Word;

class WordObserver
{
    public function creating(Word $word)
    {
        $word->letter = mb_substr($word->name, 0, 1);
        $word->description = ($word->description) ?: "";
    }

    public function saving(Word $word)
    {
        $word->letter = mb_substr($word->name, 0, 1);
    }
}