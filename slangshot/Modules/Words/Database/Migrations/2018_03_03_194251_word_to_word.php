<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class WordToWord extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('word_to_word', function (Blueprint $table) {
            $table->increments('id');
            $table->integer("parent_id")->unsigned();
            $table->integer("child_id")->unsigned();
            $table->timestamps();
        });
        Schema::table('word_to_word', function (Blueprint $table) {
            $table->foreign('parent_id')->references('id')->on('words');
            $table->foreign('child_id')->references('id')->on('words');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('word_to_word');
    }
}
