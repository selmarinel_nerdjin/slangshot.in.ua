<?php

Route::group(['prefix' => 'api', 'namespace' => 'Modules\Words\Http\Controllers', 'middleware' => ['authenticate']], function () {
    Route::get('/words', "WordsController@getWords")->name('api:words:read');
    Route::get('/words/{criterion}', "WordsController@getWord")->name('api:word:read');

    Route::post('/words', "WordsController@setWord")->middleware('guarded')->name('api:words:create');
    Route::put('/words/{id}', "WordsController@updateWord")->middleware('guarded')->name('api:words:update');
    Route::delete('/words/{id}', "WordsController@deleteWord")->middleware('guarded')->name('api:words:delete');

    Route::post('/words/{id}/approve',"WordsController@approve")->middleware('guarded')->name('api:words:approve');
    Route::post('/words/{id}/hide',"WordsController@hide")->middleware('guarded')->name('api:words:hide');

});
