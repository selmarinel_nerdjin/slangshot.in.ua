<?php

namespace Modules\Words\Http\Controllers;

use Modules\Users\Service\Exceptions\UserPermissionException;
use Modules\Words\Service\Exceptions\GuestNotAllowException;
use Modules\Words\Service\Exceptions\NotFoundWordException;
use Modules\Words\Service\Exceptions\WordValidationException;
use Validator;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Users\Service\User\User;
use Modules\Words\Service\Handle;

class WordsController extends Controller
{
    public function getAction(Request $request, $id)
    {
        $service = new Handle(new User($request->user()));
        $word = $service->getById($id);
        return new JsonResponse(["word" => $word], Response::HTTP_OK);
    }

    public function getByNameAction(Request $request, $name)
    {
        $service = new Handle(new User($request->user()));
        $word = $service->getByName($name);
        return new JsonResponse(["word" => $word], Response::HTTP_OK);
    }

    public function getWord(Request $request, $criterion)
    {
        $service = new Handle(new User($request->user()));
        $word = null;
        if (is_numeric($criterion)) {
            $word = $service->getById($criterion);
        }
        if (!$word) {
            $word = $service->getByName($criterion);
        }
        if (!$word) {
            return new JsonResponse(['message' => 'Not Found'], Response::HTTP_NOT_FOUND);
        }
        return new JsonResponse(["word" => $word], Response::HTTP_OK);
    }

    /**
     * PARAMS: ['take','letter','status','word']
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function getWords(Request $request)
    {
        $service = new Handle(new User($request->user()));
        $collection = $service->getCollection($request);

        return new JsonResponse([
            'count' => $collection->count(),
            'collection' => $collection,
            'word' => $collection->where('name', $request->input('word'))->first(),
        ], Response::HTTP_OK);
    }

    public function setWord(Request $request)
    {
        /** @var \Illuminate\Validation\Validator $validator */
        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:words,name',
            'description' => 'required',
        ]);
        if ($validator->fails()) {
            return new JsonResponse(['message' => 'Validation error', 'errors' => $validator->errors()], Response::HTTP_BAD_REQUEST);
        }
        try {
            $service = new Handle(new User($request->user()));
            $model = $service->save($request);
            return new JsonResponse(['word' => $model], Response::HTTP_CREATED);
        } catch (GuestNotAllowException $guestNotAllowException) {
            return new JsonResponse(['message' => $guestNotAllowException->getMessage()], $guestNotAllowException->getCode());
        } catch (UserPermissionException $userPermissionException){
            return new JsonResponse(['message' => $userPermissionException->getMessage()], $userPermissionException->getCode());
        }
    }

    /**
     * @param Request $request
     * @param         $id
     *
     * @return JsonResponse
     */
    public function updateWord(Request $request, $id)
    {
        /** @var \Illuminate\Validation\Validator $validator */
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'description' => 'required',
        ]);

        if ($validator->fails()) {
            return new JsonResponse(['message' => 'Validation error', 'errors' => $validator->errors()], Response::HTTP_BAD_REQUEST);
        }
        try {
            $service = new Handle(new User($request->user()));
            $model = $service->update($request, $id);
            return new JsonResponse(['word' => $model], Response::HTTP_OK);
        } catch (NotFoundWordException $notFoundWordException) {
            return new JsonResponse(['message' => $notFoundWordException->getMessage()], $notFoundWordException->getCode());
        } catch (WordValidationException $wordValidationException) {
            return new JsonResponse(['message' => $wordValidationException->getMessage()], $wordValidationException->getCode());
        } catch (UserPermissionException $userPermissionException) {
            return new JsonResponse(['message' => $userPermissionException->getMessage()], $userPermissionException->getCode());
        }
    }

    public function deleteWord(Request $request, $id)
    {
        try {
            $service = new Handle(new User($request->user()));
            if ($service->delete($id)) {
                return new JsonResponse(['message' => "Success"], Response::HTTP_NO_CONTENT);
            }
            return new JsonResponse(['message' => "Whoops. Something went wrong"], Response::HTTP_BAD_REQUEST);
        } catch (NotFoundWordException $notFoundWordException) {
            return new JsonResponse(['message' => $notFoundWordException->getMessage()], $notFoundWordException->getCode());
        } catch (UserPermissionException $userPermissionException) {
            return new JsonResponse(['message' => $userPermissionException->getMessage()], $userPermissionException->getCode());
        }
    }

    /**
     * @param Request $request
     * @param         $id
     *
     * @return JsonResponse
     */
    public function approve(Request $request, $id)
    {
        try {
            $service = new Handle(new User($request->user()));
            $word = $service->approve($id);
            return new JsonResponse(['message' => "Success", 'word' => $word], Response::HTTP_OK);
        } catch (NotFoundWordException $notFoundWordException) {
            return new JsonResponse(['message' => $notFoundWordException->getMessage()], $notFoundWordException->getCode());
        } catch (UserPermissionException $userPermissionException) {
            return new JsonResponse(['message' => $userPermissionException->getMessage()], $userPermissionException->getCode());
        }
    }

    /**
     * @param Request $request
     * @param         $id
     *
     * @return JsonResponse
     */
    public function hide(Request $request, $id)
    {
        try {
            $service = new Handle(new User($request->user()));
            $word = $service->hide($id);
            return new JsonResponse(['message' => "Success", 'word' => $word], Response::HTTP_OK);
        } catch (NotFoundWordException $notFoundWordException) {
            return new JsonResponse(['message' => $notFoundWordException->getMessage()], $notFoundWordException->getCode());
        } catch (UserPermissionException $userPermissionException) {
            return new JsonResponse(['message' => $userPermissionException->getMessage()], $userPermissionException->getCode());
        }
    }
}
