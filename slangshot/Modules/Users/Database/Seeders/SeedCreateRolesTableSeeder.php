<?php

namespace Modules\Users\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Users\Entities\Role;

class SeedCreateRolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        Role::query()->create([
            'id' => Role::ADMIN,
            'name' => 'admin'
        ]);
        Role::query()->create([
            'id' => Role::USER,
            'name' => 'user'
        ]);
        Role::query()->create([
            'id' => Role::VERIFIED_USER,
            'name' => 'verified user'
        ]);
        Role::query()->create([
            'id' => Role::MODERATOR,
            'name' => 'moderator'
        ]);
    }
}
