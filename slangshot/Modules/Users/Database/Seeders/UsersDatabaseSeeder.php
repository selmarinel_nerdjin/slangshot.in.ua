<?php

namespace Modules\Users\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Users\Entities\UserModel;

class UsersDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        UserModel::query()->create([
            'id' => 1,
            'name' => 'Admin',
            'email' => 'selmarinel@gmail.com',
            'password' => bcrypt(123123)
        ]);

        $this->call(SeedCreateRolesTableSeeder::class);
    }
}
