<?php

namespace Modules\Users\Http\Controllers\Auth;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use JWTAuth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Modules\Users\Entities\UserModel;
use Modules\Users\Service\Auth\AuthHandler;
use Validator;

class ManualRegistrationController extends Controller
{

    public function registrationAction(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email|unique:users|max:255',
            'password' => 'required',
            'name' => 'required',
        ]);

        if ($validator->fails()) {
            return new JsonResponse([
                'message' => 'Validation Fail',
                'errors' => $validator->errors(),
            ], Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        /** @var AuthHandler $auth */
        $auth = app(AuthHandler::class);
        /** @var UserModel $user */
        $user = $auth->createFromRequest($request);

        $token = JWTAuth::fromUser($user);
        return response()->json(compact('token'));
    }
}