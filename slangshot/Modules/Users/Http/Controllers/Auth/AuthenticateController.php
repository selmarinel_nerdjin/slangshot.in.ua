<?php

namespace Modules\Users\Http\Controllers\Auth;


use App\Http\Controllers\Controller;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use JWTAuth;
use Modules\Users\Service\Auth\FacebookAuthHandler;
use Socialite;
use Tymon\JWTAuth\Exceptions\JWTException;

class AuthenticateController extends Controller
{
    public function authenticate(Request $request)
    {
        $credentials = $request->only('email', 'password');
        try {
            if (!$token = JWTAuth::attempt($credentials)) {
                return response()->json(['error' => 'invalid_credentials'], 401);
            }
        } catch (JWTException $e) {
            return response()->json(['error' => 'could_not_create_token'], 500);
        }
        return response()->json(compact('token'));
    }

    public function facebookAuthenticate(Request $request)
    {
        $token = $request->input('token');
        try {
            $socialUser = Socialite::driver('facebook')->userFromToken($token);
            $service = new FacebookAuthHandler();
            $user = $service->createUserFromSocial($socialUser);
            return response()->json(compact('token'));
        } catch (ClientException $clientException) {
            return response()->json(['message' => 'Can\'t connect to facebook'], Response::HTTP_BAD_REQUEST);
        }

    }
}