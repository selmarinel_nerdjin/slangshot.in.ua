<?php

namespace Modules\Users\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Users\Entities\Role;
use Modules\Users\Entities\UserModel;
use Modules\Users\Service\Exceptions\UserPermissionException;
use Modules\Users\Service\Handle;
use Modules\Users\Service\User\User;

class UserController extends Controller
{

    public function getUser(Request $request, $id)
    {
        $service = new Handle(new User($request->user()));
        $user = $service->getOneById($id);
        if (!$user) {
            return new JsonResponse(["message" => "User not found"], Response::HTTP_NOT_FOUND);
        }
        return new JsonResponse(["user" => $user], Response::HTTP_OK);
    }

    public function getUsers(Request $request)
    {
        $service = new Handle(new User($request->user()));
        try {
            $collection = $service->getUsers();
            return new JsonResponse(['users' => $collection], Response::HTTP_OK);
        } catch (UserPermissionException $userPermissionException) {
            return new JsonResponse(['message' => $userPermissionException->getMessage()], $userPermissionException->getCode());
        }
    }
}