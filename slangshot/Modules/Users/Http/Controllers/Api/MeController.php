<?php

namespace Modules\Users\Http\Controllers\Api;

use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class MeController extends Controller
{
    public function getMeAction(Request $request)
    {
        return new JsonResponse(['user' => $request->user()], Response::HTTP_OK);
    }

    public function updateMeAction(Request $request)
    {
        /** @var \Illuminate\Validation\Validator $validator */
        $validator = Validator::make($request->all(), [
            'name' => 'required',
        ]);

        if ($validator->fails()) {
            return new JsonResponse(['message' => 'Validation error', 'errors' => $validator->errors()], Response::HTTP_BAD_REQUEST);
        }
        $request->user()->name = $request->input('name');
        $request->user()->save();
        return new JsonResponse(['user' => $request->user()], Response::HTTP_OK);
    }
}