<?php

Route::group(['prefix' => 'api', 'namespace' => 'Modules\Users\Http\Controllers'], function () {

    Route::post('/auth', 'Auth\AuthenticateController@authenticate')->name('api:auth');
    Route::post('/facebook/auth', 'Auth\AuthenticateController@facebookAuthenticate')->name('api:facebook:auth');

    Route::post('/register', 'Auth\ManualRegistrationController@registrationAction')->name('api:register');

    Route::middleware(['authenticate'])->group(function () {
        Route::get('/users/{id}', 'Api\UserController@getUser')->name('api:user:get');
        Route::get('/users', 'Api\UserController@getUsers')->name('api:users:get');
    });
    
    Route::middleware(['guarded'])->prefix('/user')->group(function () {
        Route::get('/', 'Api\MeController@getMeAction')->name('api:user:me:read');
        Route::put('/', 'Api\MeController@updateMeAction')->name('api:user:me:update');
    });
});
