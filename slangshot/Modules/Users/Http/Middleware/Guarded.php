<?php

namespace Modules\Users\Http\Middleware;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Closure;

class Guarded
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = $request->user();
        if (!$user) {
            return new JsonResponse(['message' => 'Invalid credentials'], Response::HTTP_UNAUTHORIZED);
        }
        return $next($request);
    }
}
