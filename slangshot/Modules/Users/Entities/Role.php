<?php
/**
 * Created by PhpStorm.
 * User: selma
 * Date: 14.02.2018
 * Time: 18:20
 */

namespace Modules\Users\Entities;


use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $fillable = [
        "id",
        "name"
    ];

    const ADMIN = 1;
    const USER = 2;
    const VERIFIED_USER = 3;
    const MODERATOR = 4;

    public function users()
    {
        return $this->belongsToMany(UserModel::class,'user_has_role','role_id', 'user_id', 'id', 'id');
    }
}