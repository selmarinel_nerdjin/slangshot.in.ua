<?php
/**
 * Created by PhpStorm.
 * User: selma
 * Date: 15.02.2018
 * Time: 16:36
 */

namespace Modules\Users\Entities;


use Illuminate\Database\Eloquent\Model;

/**
 * @property \Carbon\Carbon $created_at
 * @property int $id
 * @property \Carbon\Carbon $updated_at
 * @property UserModel $user
 */
class SocialAccount extends Model
{
    protected $table = 'social_accounts';

    protected $fillable = ['user_id', 'provider_user_id', 'provider'];

    public function user()
    {
        return $this->belongsTo(UserModel::class);
    }
}