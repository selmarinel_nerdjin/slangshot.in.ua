<?php

namespace Modules\Users\Entities;


use App\User;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Tymon\JWTAuth\Contracts\JWTSubject;

/**
 * Class UserModel
 * @package App\Models
 * @property \Carbon\Carbon $created_at
 * @property int $id
 * @property \Carbon\Carbon $updated_at
 * @property BelongsToMany $roles
 */
class UserModel extends User implements JWTSubject
{
    protected $table = 'users';

    public function roles()
    {
        return $this->belongsToMany(Role::class, 'user_has_role', 'user_id', 'role_id', 'id', 'id');
    }


    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [];
    }
}