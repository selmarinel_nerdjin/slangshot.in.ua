<?php

namespace Modules\Users\Service\User;


use Modules\Users\Entities\Role;
use Modules\Users\Entities\UserModel;
use Modules\Users\Service\Roles\AdminRole;
use Modules\Users\Service\Roles\GuestRole;
use Modules\Users\Service\Roles\ModeratorRole;
use Modules\Users\Service\Roles\RoleInterface;
use Modules\Users\Service\Roles\UserRole;
use Modules\Users\Service\Roles\VerifiedRole;

class User
{
    /** @var UserModel|null */
    private $model = null;
    /** @var RoleInterface */
    private $role;

    public function __construct(UserModel $userModel = null)
    {
        if (!$userModel) {
            $this->model = new UserModel();
            $this->role = new GuestRole();
        } else {
            $this->model = $userModel;
        }
    }

    /**
     * @return RoleInterface
     */
    public function getRole()
    {
        if ($this->isAdmin()) {
            $this->role = new AdminRole();
        } elseif ($this->isModerator()) {
            $this->role = new ModeratorRole();
        } elseif ($this->isVerifiedUser()) {
            $this->role = new VerifiedRole();
        } else {
            $this->role = new UserRole();
        }
        return $this->role;
    }

    public function getId()
    {
        return (int)$this->model->id;
    }

    private function isAdmin()
    {
        return (bool)$this->model->roles->where('id', Role::ADMIN)->count();
    }

    private function isVerifiedUser()
    {
        return (bool)$this->model->roles->where('id', Role::VERIFIED_USER)->count();
    }

    private function isModerator()
    {
        return (bool)$this->model->roles->where('id', Role::MODERATOR)->count();
    }
}