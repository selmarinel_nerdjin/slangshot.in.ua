<?php
/**
 * Created by PhpStorm.
 * User: selma
 * Date: 15.02.2018
 * Time: 15:36
 */

namespace Modules\Users\Service\Exceptions;


use Illuminate\Http\Response;

class UserPermissionException extends \Exception
{
    public $message = 'You don\'t have permission';

    public $code = Response::HTTP_FORBIDDEN;
}