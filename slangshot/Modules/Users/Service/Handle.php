<?php

namespace Modules\Users\Service;

use Modules\Users\Entities\UserModel;
use Modules\Users\Service\Exceptions\UserPermissionException;
use Modules\Users\Service\Roles\AdminRole;
use Modules\Users\Service\User\User;

class Handle
{
    /** @var User */
    private $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function getOneById($id)
    {
        return UserModel::query()->find($id);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|UserModel[]
     * @throws UserPermissionException
     */
    public function getUsers()
    {
        if ($this->user->getRole() instanceof AdminRole) {
            return UserModel::query()->get();
        }
        throw new UserPermissionException;
    }

    public function changeRole($id, $role)
    {
        $user = UserModel::query()->find($id);
    }

}