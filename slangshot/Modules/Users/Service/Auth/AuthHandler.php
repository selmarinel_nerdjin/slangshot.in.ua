<?php

namespace Modules\Users\Service\Auth;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Modules\Users\Entities\Role;
use Modules\Users\Entities\UserModel;

class AuthHandler
{
    public function createFromRequest(Request $request)
    {
        $userModel = new UserModel();
        $userModel->email = $request->input('email');
        $userModel->name = $request->input('name');
        $userModel->password = bcrypt($request->input('password'));

        $userModel->save();
        $userModel->roles()->attach(Role::USER,
            [
                'created_at' => new Carbon(),
                'updated_at' => new Carbon(),
            ]
        );
        return $userModel;
    }
}