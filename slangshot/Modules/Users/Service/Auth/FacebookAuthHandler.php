<?php
/**
 * Created by PhpStorm.
 * User: selma
 * Date: 15.02.2018
 * Time: 16:45
 */

namespace Modules\Users\Service\Auth;

use Carbon\Carbon;
use DB;
use Faker\Provider\Lorem;
use Laravel\Socialite\Two\User;
use Modules\Users\Entities\Role;
use Modules\Users\Entities\SocialAccount;
use Modules\Users\Entities\UserModel;

class FacebookAuthHandler
{
    public function createUserFromSocial(User $user)
    {
        /** @var SocialAccount $account */
        $account = SocialAccount::query()
            ->where('provider', 'facebook')
            ->where('provider_user_id', $user->getId())
            ->first();
        if ($account) {
            return $account->user;
        }

        $userModel = new UserModel();
        $userModel->email = $user->getEmail();
        $userModel->name = $user->getName();
        $userModel->password = bcrypt(Lorem::word());

        $userModel->save();
        $userModel->roles()->attach(Role::USER,
            [
                'created_at' => new Carbon(),
                'updated_at' => new Carbon()
            ]
        );

        $account = new SocialAccount();
        $account->user_id = $userModel->id;
        $account->provider_user_id = $user->getId();
        $account->provider = 'facebook';
        $account->save();

        return $userModel;
    }
}