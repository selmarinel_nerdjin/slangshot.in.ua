<?php

namespace App\Service\WordsPuller;


use App\Service\WordsPuller\Rules\GuestRule;
use App\Service\WordsPuller\Rules\RuleInterface;
use App\User;
use Illuminate\Http\Request;

class Garbage
{
    /** @var User|null */
    private $user;

    /** @var RuleInterface */
    private $rule;

    public function __construct(Request $request)
    {
        $this->user = $request->user();
    }

    /**
     * @return User|null
     */
    public function getUser(): ?User
    {
        return $this->user;
    }

    public function identifyUserRule()
    {
        if(!$this->user){
            $this->rule = new GuestRule();
        }

    }
}