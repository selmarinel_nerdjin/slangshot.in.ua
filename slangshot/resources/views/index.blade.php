<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Title</title>
    <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Roboto:400,500,700,400italic|Material+Icons">
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    @yield('style')
</head>
<body>
<div id="app">
    <md-toolbar class="md-accent" md-elevation="1">
        <h3 class="md-title" style="flex: 1">Slangshot</h3>
        <md-button>
            <router-link to="/foo">Go to Foo</router-link>
        </md-button>
        <md-button class="md-primary">
            <router-link to="/bar">Go to Bar</router-link>
        </md-button>
    </md-toolbar>


    <transition name="slide-left">
        <router-view></router-view>
    </transition>
</div>

<script src="{{ asset('js/app.js') }}"></script>

</body>
</html>